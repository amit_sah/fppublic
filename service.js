/**
 * Service entry point
 */
'use strict'
if (process.env.ENABLE_TRACE) {
  require('@google-cloud/trace-agent').start()
}
require('colors')
const express = require('express')
const http = require('http')
const bodyParser = require('body-parser')
const cors = require('cors')
const gracefulexit = require('./sys/gracefulexit')
const log = require('./sys/log')
const app = express()

// Load env from file for dev. Set NODE_ENV in your bashrc or zshrc.
if (process.env.NODE_ENV === 'development') {
  require('env2')('./devenv.json')
}

//Initalize and resume SQS & RabbitMQ queue
require('./sys/taskinit')

let {
  sysnats,
  rxnats,
  grpcnorthsouthserver,
  grpceastwestserver,
  grpcclient,
  getrabbitmqconfig
} = require('./sys/servicewrapper')()

const diagnosticsrouter = require('./routers/servicediagnostics')
app.use(bodyParser.json())
app.use(bodyParser.raw())
app.use(cors())

/**
 * Methods bind on /dignostics
 * Service dignostics
 */
app.use('/diagnostics/', diagnosticsrouter)

// Routers
const contentrouter = require('./routers/content')
const approuter = require('./routers/app')
app.use('/api/content/', contentrouter)
app.use('/', approuter)
app.use('/api/store', require('./routers/store.route'))
app.use('/api/deeplink', require('./routers/deeplink'))

/*
// Routes Tracking
app.use((req, res, next) => {
  const { status, data } = res.locals
  const { headers: metadata, body: request, url: channel, method } = req
  if (process.env.NODE_ENV === 'staging' && _.has(metadata, 'x-breeze-id')) {
    const Recorder = require('./sys/middleware/recorder')
    const recorder = new Recorder()
    recorder.save(recorder.capture({ channel, method }, { request, metadata }, { status, data }))
  }
  res.status(status || 200).json(data)
  next()
})*/

const port = process.env.SERVICE_PORT || 3000
const server = http.createServer(app)
server.listen(port, () => {
  log(`REST serving on port ${port}`.green.bold)
})

/**
 * Manual Graceful service exit on SIGTERM, SIGINT events
 * Finish RXNATS, gRPC, REST processes before termination.
 * Stop Cache & MySQl connection
 * Forward RabbitMQ publisher's & subscriber's channels & connections
 */
gracefulexit(sysnats, rxnats, server, grpcnorthsouthserver, grpceastwestserver, getrabbitmqconfig)
