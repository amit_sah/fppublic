const _ = require('lodash')

class deeplink {
  getmapping(callback) {
    let data = []
    _.each(`${process.env.SHORTLINK_MAPPING}`.split(','), v => {
      v = v.split(':')
      let d = {
        mapping: v.pop(),
        event: v
      }
      data.push(d)
    })
    callback({ status: 'success', data })
  }
}

module.exports = new deeplink()
