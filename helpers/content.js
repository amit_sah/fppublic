const async = require('async')
const log = require('../sys/log')
const Content = require('../persistence/content')
const comms = require('../sys/comms')
const grpc = require('../sys/grpc')
const _ = require('lodash')
const moment = require('moment')
const { Parser } = require('json2csv')

class helper {
  getbannersequences(incoming, callback) {
    let clienterr,
      syserr,
      response = {}

    let getsequences = (_callback) => {
      Content.getbannersequences((err, res) => {
        if (err) {
          syserr = `Failed to get banner sequences ${err}`
          clienterr = 'Failed. Try again.'
        } else response = res

        _callback(clienterr)
      })
    }

    let formatdata = (_callback) => {
      try {
        response = JSON.parse(response)
      } catch (error) {
        clienterr = 'Failed to get sequences. Please try again.'
        syserr = 'Invalid JSON ' + res
      } finally {
        _callback(clienterr)
      }
    }

    async.waterfall([getsequences, formatdata], (err) => {
      if (err || clienterr) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: response ? response : [] })
      }
    })
  }

  getpartners(callback) {
    let response = [
      { id: 1, walletid: '01', name: 'Big Bazaar' },
      { id: 4, walletid: '01', name: 'FBB' },
      { id: 5, walletid: '01', name: 'HyperCity' },
      { id: 6, walletid: '01', name: 'FoodHall' },
      { id: 2, walletid: '01', name: 'EZone' },
      { id: 1, walletid: '02', name: 'Central' },
      { id: 2, walletid: '02', name: 'All' },
      { id: 1, walletid: '03', name: 'Brand Factory' },
      { id: 1, walletid: '08', name: 'Easyday club' },
      { id: 2, walletid: '08', name: 'Heritage fresh' },
      { id: 1, walletid: '11', name: 'Home Town' }
    ]

    callback({ status: 'success', data: response })
  }

  getwalletpartners(walletid, callback) {
    let response = null
    switch (walletid) {
      case '01':
        response = [
          { id: 1, name: 'Big Bazaar' },
          { id: 4, name: 'FBB' },
          { id: 5, name: 'HyperCity' },
          { id: 6, name: 'FoodHall' },
          { id: 2, name: 'EZone' },
        ]
        break
      case '02':
        response = [
          { id: 1, name: 'Central' },
          { id: 2, name: 'All' },
        ]
        break
      case '03':
        response = [{ id: 1, name: 'Brand Factory' }]
        break
      case '04':
        response = [{ id: 1, name: 'BBPC' }]
        break
      case '05':
        response = [{ id: 1, name: 'Payback' }]
        break
      case '06':
        response = [
          { id: 1, name: 'Big Bazaar' },
          { id: 4, name: 'FBB' },
          { id: 5, name: 'HyperCity' },
        ]
        break
      case '08':
        response = [
          { id: 1, name: 'Easyday club' },
          { id: 2, name: 'Heritage fresh' },
        ]
        break
      case '11':
        response = [{ id: 1, name: 'Home Town' }]
        break
      default:
        response = []
    }

    callback({ status: 'success', data: response })
  }

  getformats(callback) {
    let data = [
      { id: 1, name: 'Big Bazaar' },
      { id: 2, name: 'Central' },
      { id: 3, name: 'Ezone' },
      { id: 4, name: 'Brand Factory' },
      { id: 5, name: 'Easyday' },
    ]
    callback({ status: 'success', data: data })
  }

  getbannersupdatenonce(callback) {
    let lastupdatednonce = Date.now().toString()
    Content.getbannersupdatenonce((err, res) => {
      if (err) log(` Error occired in redis ${err}`)
      callback({ status: 'success', lastupdatednonce: res ? res : lastupdatednonce })
    })
  }

  getbanners(callback) {
    let clienterr, syserr
    let banners = null
    let lastupdatednonce = Date.now().toString()
    let updateintervalsec = process.env.BANNERS_NONCE_CHECK_INTERVAL ? process.env.BANNERS_NONCE_CHECK_INTERVAL : 1

    Content.getbannersupdatenonce((err, res) => {
      if (err) {
        clienterr = `Failed. Try again. ${err}`
      } else {
        if (res) lastupdatednonce = res
      }
    })

    Content.getbanners((err, res) => {
      if (err) {
        clienterr = 'Failed. Try again.'
        syserr = err
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        banners = res.map((item) => {
          item.creativeid = `F${moment(item.created_at).format('YY')}${moment(item.created_at).format('MM')}${item.id}`
          item.type = item.type ? item.type : 'public'
          item.subtype = item.type === 'public' || item.type === 'scratchcard' ? 'p' : 'k'
          return item
        })
        callback({ status: 'success', lastupdatednonce, updateintervalsec: Number(updateintervalsec), data: banners })
      }
    })
  }

  getkritterbanners(callback) {
    let clienterr, syserr
    let banners = null

    Content.getkritterbanners((err, res) => {
      if (err) {
        clienterr = 'Failed. Try again.'
        syserr = err
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        banners = res.map((item) => {
          item.creativeid = `F${moment(item.created_at).format('YY')}${moment(item.created_at).format('MM')}${item.id}`
          item.type = item.type ? item.type : 'personalized automated'
          item.subtype = item.type === 'personalized automated' ? 'k' : 'p'
          return item
        })
        callback({ status: 'success', data: banners })
      }
    })
  }

  getintroscreens(callback) {
    let clienterr, syserr

    Content.getintroscreens((err, res) => {
      if (err) {
        clienterr = 'Failed. Try again.'
        syserr = err
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: res })
      }
    })
  }

  getshopbanners(callback) {
    let clienterr, syserr
    let lastupdatednonce = Date.now().toString()
    let updateintervalsec = process.env.BANNERS_NONCE_CHECK_INTERVAL ? process.env.BANNERS_NONCE_CHECK_INTERVAL : 1

    Content.getshopbannersupdatenonce((err, res) => {
      if (err) {
        clienterr = `Failed. Try again. ${err}`
      } else {
        if (res) lastupdatednonce = res
      }
    })

    Content.getshopbanners((err, res) => {
      if (err) {
        clienterr = 'Failed. Try again.'
        syserr = err
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', lastupdatednonce, updateintervalsec: Number(updateintervalsec), data: res })
      }
    })
  }

  getshopbannersupdatenonce(callback) {
    let lastupdatednonce = Date.now().toString()
    Content.getshopbannersupdatenonce((err, res) => {
      if (err) log(` Error occired in redis ${err}`)
      callback({ status: 'success', lastupdatednonce: res ? res : lastupdatednonce })
    })
  }

  getshopbycategory(callback) {
    let clienterr, syserr

    Content.getshopbycategory((err, res) => {
      if (err) {
        clienterr = 'Failed. Try again.'
        syserr = err
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: res })
      }
    })
  }

  getbrands(callback) {
    let clienterr, syserr

    Content.getbrands((err, res) => {
      if (err) {
        clienterr = 'Failed. Try again.'
        syserr = err
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: res })
      }
    })
  }

  getwalletreport(fphash, callback) {
    try {
      let clienterr, syserr
      let fpdetails = JSON.parse(Buffer.from(fphash, 'base64').toString())
      let fbformat = {
        'ED-SF': 'Easy Day',
        FH: 'FoodHall',
        BB: 'Big Bazaar',
        BF: 'BrandFactory',
        CT: 'Central',
        HF: 'Heritage Fresh',
        AD: 'Aadhaar',
        FBB: 'Fbb',
        AL: 'aLL',
        EZ: 'Ezone',
        FB: 'Future Bazaar',
        ES: 'Early Salary',
        BD: 'Big Bazaar Direct',
        HC: 'HyperCity',
        FGNG: 'Future Pay Future Retail Home Office',
        FTB: 'Future Bazaar',
        HT: 'Home Town'
      }
      Content.getwalletreport(fpdetails, (err, res) => {
        if (err) {
          clienterr = 'Failed. Try again.'
          syserr = err
          log(syserr || err, true)
          callback({ status: 'failed', message: clienterr })
        } else {
          let fields = [
            'Time',
            'Activity',
            'Format',
            'Store Details',
            'Wallet Transaction ID',
            'Debit',
            'Credit',
            'Balance',
          ]
          let walletreport = {}
          _.forEach(res, (row) => {
            if (!walletreport[row.wallet_name]) {
              walletreport[row.wallet_name] = []
            }
            let line = {
              Time: moment(row.time).format('DD/MM/YY h:mm:ss a'),
              Activity: row.activity,
              Format: fbformat[row.format],
              'Store Details': row.store,
              'Wallet Transaction ID': 'TX: ' + row.txnid,
              Debit: row.debit,
              Credit: row.credit,
              Balance: row.balance,
            }
            walletreport[row.wallet_name].push(line)
          })

          const parser = new Parser()
          let csv = ''

          _.forEach(walletreport, (value, key) => {
            csv += key + '\n'
            csv += parser.parse(value)
            csv += '\n\n'
          })

          callback({
            status: 'success',
            data: csv,
            filename: `Futurepay_Statement_${fpdetails.omnt}_${fpdetails.oyr}.csv`,
          })
        }
      })
    } catch (err) {
      callback({ status: 'failed', message: 'Invalid Request' })
    }
  }

  widgetupdatenonce(callback) {
    let lastupdatednonce = Date.now().toString()
    Content.widgetupdatenonce((err, res) => {
      if (err) log(` Error occired in redis ${err}`)
      callback({ status: 'success', lastupdatednonce: res ? res : lastupdatednonce })
    })
  }

  getshopwidgets(incoming, callback) {
    if (incoming.section) incoming.section = incoming.section.trim()
    if (incoming.section && ![`shop`, `futuremoney`, `pay`].includes(incoming.section)) {
      log(`Invalid section ${incoming.section}`, true)
      return callback({ status: 'failed', message: `Invalid section name` })
    }
    let section = `shop`
    if (incoming.section) section = incoming.section
    let clienterr, syserr
    let lastupdatednonce = Date.now().toString()
    let updateintervalsec = process.env.WIDGET_UPDATE_INTERVAL ? Number(process.env.WIDGET_UPDATE_INTERVAL) : 60
    let shopwidgets = []

    function getupdatetime(_callback) {
      Content.widgetupdatenonce((err, res) => {
        if (err) {
          clienterr = `Failed. Try again. ${err}`
        } else {
          if (res) lastupdatednonce = res
        }
        _callback(null)
      })
    }
    function fetchwidgets(_callback) {
      Content.getwidgetlist(section, (err, resp) => {
        if (err || !resp) {
          clienterr = 'Failed. Try again.'
          syserr = err
          _callback(err)
        } else {
          let widgetids = []
          resp.forEach(widget => {
            widgetids.push(widget.id)
            shopwidgets.push({
              widgetid: widget.id,
              seqnumber: widget.seqnumber,
              widgetname: widget.name,
              widgettype: widget.type,
              headertext: widget.headertext,
              buttontext: widget.buttontext,
              buttoncategorytype: widget.buttoncategorytype,
              buttoncategorylink: widget.buttoncategorylink,
              category: widget.category,
              channeltype: widget.channeltype,
              subtype: widget.subtype,
              section: widget.section,
              subwidgets: []
            })
          })
          _callback(null, widgetids)
        }
      })
    }
    function fetchsubwidgets(widgetids, _callback) {
      if (widgetids && widgetids.length) {
        Content.getsubwidgets_by_widgetids(widgetids, section, (err, resp) => {
          if (err || !resp) {
            clienterr = 'Failed. Try again.'
            syserr = err
            _callback(err)
          } else {
            shopwidgets.forEach(widget => {
              const swdlist = resp.filter(swd => swd.widgetid == widget.widgetid)
              swdlist.forEach(swd => {
                const subwidget = {
                  subwidgetid: swd.id,
                  subwidgetname: swd.name,
                  seqnumber: swd.seqnumber,
                  imagelink: swd.imagelink || '',
                  categorysubtype: swd.categorysubtype,
                  categorysubtypelink: "",
                  channeltype: swd.channeltype,
                  campaignname: swd.campaignname || '',
                  tags: swd.tags || '',
                  subtype: swd.subtype,
                  section: widget.section,
                  external: swd.external
                }
                if (swd.categorysubtype == "weblink" || swd.categorysubtype == "deeplink")
                  subwidget.categorysubtypelink = swd.deeplink
                else if (swd.categorysubtype == "pwa")
                  subwidget.categorysubtypelink = swd.pwalink
                else subwidget.categorysubtypelink = swd.htmllink

                widget.subwidgets.push(subwidget)
              })
            })
            _callback(null)
          }
        })
      } else _callback(null)
    }
    function sliceblank(_callback) {
      let temparr = []
      shopwidgets.forEach(widget => {
        if (widget && widget.subwidgets.length) {
          temparr.push(widget)
        }
      })
      shopwidgets = temparr
      _callback(null)
    }
    async.waterfall([getupdatetime, fetchwidgets, fetchsubwidgets, sliceblank], error => {
      if (error) {
        log(`Get shop widgets error: ${error || syserr}`, true)
        callback({ status: 'failed', message: clienterr })
      }
      else
        callback({
          status: 'success',
          lastupdatednonce,
          updateintervalsec: Number(updateintervalsec),
          shopwidgets: shopwidgets
        })
    })
  }

  gethamburger(callback) {
    let clienterr, syserr

    Content.gethamburger((err, res) => {
      if (err) {
        clienterr = 'Failed. Try again.'
        syserr = err
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: res })
      }
    })
  }

  getshoponline(callback) {
    let syserr, clienterr, subwidgets = []
    Content.getshoponline((err, resp) => {
      if (err || !resp) {
        clienterr = 'Failed. Try again.'
        syserr = err
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        resp.forEach(swd => {
          const subwidget = {
            id: swd.id,
            name: swd.name,
            subtype: swd.categorysubtype,
            subtypelink: "",
            channeltype: swd.channeltype,
            campaignname: swd.campaignname || '',
            tags: swd.tags || '',
            toolbartext: swd.toolbartext || '',
          }
          if (swd.categorysubtype == "weblink" || swd.categorysubtype == "deeplink") {
            subwidget.subtypelink = swd.deeplink
          } else if (swd.categorysubtype == "pwa") {
            subwidget.subtypelink = swd.pwalink
          } else subwidget.subtypelink = swd.htmllink
          subwidgets.push(subwidget)
        })
        callback({ status: 'success', data: subwidgets })
      }
    })
  }
}

module.exports = new helper()
