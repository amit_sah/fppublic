const log = require('../sys/log')
const StorePersistence = require('../persistence/store.persistence')

class StoreHelper {
  getstorebycity(req, res, next) {
    if (!req.query.city) {
      return res.json({ status: 'failed', message: `City name has required.` })
    }
    let city = req.query.city.trim()
    StorePersistence.executegetstorequery(city, (err, data) => {
      if (err) {
        log(err, true)
        return res.json({ status: 'failed', message: err.message })
      } else {
        let stores = []
        data.forEach(store => {
          stores.push({
            storecode: store.storecode,
            name: store.name,
            address: store.address
              ? store.address
              : '' + ' ' + store.city
                ? store.city
                : '' + ' ' + store.state
                  ? store.state
                  : '',
            latitude: store.latitude,
            longitude: store.longitude
          })
        })
        return res.json({ stores })
      }
    })
  }

  getpmstoresbycity(req, res, next) {
    if (!req.query.city) {
      return res.json({ status: 'failed', message: `City name has required.` })
    }
    let city = req.query.city.trim()
    StorePersistence.executegetpmstorequery(city, (err, data) => {
      if (err) {
        log(err, true)
        return res.json({ status: 'failed', message: err.message })
      } else {
        let stores = []
        data.forEach(store => {
          stores.push({
            storecode: store.storecode,
            name: store.name
          })
        })
        return res.json({ stores })
      }
    })
  }

  getcities(req, res, next) {
    StorePersistence.getcities((err, data) => {
      if (err) {
        log(err, true)
        return res.json({ status: 'failed', message: err.message })
      } else {
        let cities = []
        data.forEach(obj => cities.push(obj.city))
        return res.json({ cities: cities })
      }
    })
  }

  getpmcities(req, res, next) {
    StorePersistence.getpmcities((err, data) => {
      if (err) {
        log(err, true)
        return res.json({ status: 'failed', message: err.message })
      } else {
        let cities = []
        data.forEach(obj => cities.push(obj.city))
        return res.json({ cities: cities })
      }
    })
  }

  getstorestate(req, res, next) {
    StorePersistence.getstorestate((err, data) => {
      if (err) {
        log(`Get Store state name err: ${err}`, true)
        return res.json({ status: 'failed', message: err.message })
      } else {
        let states = []
        data.forEach(obj => states.push(obj.state))
        return res.json({ status: "success", states: states })
      }
    })
  }

  getstoresbylocation(req, res, next) {
    if (!(req.query.longitude && req.query.latitude)) {
      return res.json({ status: `failed`, message: `Longitude or latitude doesn't exists.` })
    }

    StorePersistence.getstoresbylocation(req, (err, data) => {
      if (err) {
        log(err, true)
        return res.json({ status: 'failed', message: err.message })
      } else {
        return res.json({ stores: data })
      }
    })
  }

  getstoresbylocationv2(req, res) {
    if (!(req.query.longitude && req.query.latitude)) {
      return res.json({ status: `failed`, message: `longitude and latitude required` })
    }
    else if (isNaN(req.query.longitude)) return res.json({ status: `failed`, message: `Invalid longitude` })
    else if (isNaN(req.query.latitude)) return res.json({ status: `failed`, message: `Invalid latitude` })

    if (!req.query.format) {
      return res.json({ status: `failed`, message: `Format name is required` })
    }

    StorePersistence.getstoresbylocationv2(req, (err, response) => {
      if (err) {
        log(`Get stores by lat & long. error: ${err}`, true)
        return res.json({ status: 'failed', message: err.details || err.message })
      } else {
        return res.json(response)
      }
    })
  }

}

module.exports = new StoreHelper()
