/**
 * List of scheduled task/job runs through service auto scaler
 * for example on @prepaid, `expirecashback` & `bulkcashback`
 * Below list of task used to create conn & channels for publish
 * default tasks = [] at for each service
 */
const tasks = []

module.exports = tasks
