const express = require('express')
const router = express.Router()
const path = require('path')

router.get('/download', (req, res) => {
  res.sendFile(path.resolve(__dirname, '../views/download.html'))
})


module.exports = router