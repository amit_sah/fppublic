const express = require('express')
const cachedexamplerouter = express.Router()
const pjson = require('../package.json')

// CHECK CACHE MIDDLEWARE

// REAL VERSION
cachedexamplerouter.get('/how', (req, res) => {
  let realdata = 'This is mock data'
  res.send(realdata)
})

cachedexamplerouter.get('/how2', (req, res) => {
  let realdata = 'This is mock data 2'
  res.send(realdata)
})

cachedexamplerouter.post('/how2', (req, res, next) => {
  let realdata = req.body
  res.locals.data = realdata
  res.send(realdata)
  next()
})

module.exports = cachedexamplerouter
