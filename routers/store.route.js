const express = require('express');
const router = express.Router()
const storeHelper = require('../helpers/store.helper')
/**
 * Get stores by city name
 * Staging API URL: /api/store/getstoresbycity?city=Mumbai
 * query params city: required
 * Query from stores.
 */
router.get('/getstoresbycity', storeHelper.getstorebycity)

/**
 * Get Only enable `pricematch` stores by city name
 * Staging API URL: /api/store/getpmstoresbycity?city=Mumbai
 * query params city: required
 * Query from stores.
 */
router.get('/getpmstoresbycity', storeHelper.getpmstoresbycity)

/**
 * Get list of city where stores exists.
 * Staging API URL: /api/store/getcities
 * No arguments has required.
 * Query from stores.
 */
router.get('/getcities', storeHelper.getcities)

/**
 * Get list of city where stores exists & pricematch='Y'.
 * Staging API URL: /api/store/getpmcities
 * No arguments has required.
 * Query from stores.
 */
router.get('/getpmcities', storeHelper.getpmcities)

/**
 * Get list of store's state name in ASC order
 */
router.get('/getstorestate', storeHelper.getstorestate)

/**
 * Get stores details by lat long
 * Staging API URL: /api/store/getstorebylocation?longitude=13.101&latitude=30.2201010
 * query params: longitude(decimal):required, latidude(decimal):required, range(decimal | int): optional
 * Search from sysradis.
 */
router.get('/getstorebylocation', storeHelper.getstoresbylocation)

/**
 * Get stores details by lat & long
 * Staging API URL: /api/store/v2/getstorebylocation?longitude=13.101&latitude=30.2201010&format="BB"
 * query params: longitude(decimal):required, latidude(decimal):required, range(decimal | int): optional
 * Search from sysradis.
 */
router.get('/v2/getstorebylocation', storeHelper.getstoresbylocationv2)

module.exports = router