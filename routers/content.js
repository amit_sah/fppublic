const express = require('express')
const router = express.Router()
const helper = require('../helpers/content')
const log = require('../sys/log')

router.get('/getbannersequences', (req, res) => {
  helper.getbannersequences(req.body, (response) => {
    res.send(response)
  })
})

router.get('/getbannersupdatenonce', (req, res) => {
  helper.getbannersupdatenonce((response) => {
    res.send(response)
  })
})

router.get('/getbanners', (req, res) => {
  helper.getbanners((response) => {
    res.send(response)
    log(`/getbanners, status:${response.status}`)
  })
})

router.get('/getkritterbanners', (req, res) => {
  helper.getkritterbanners((response) => {
    res.send(response)
  })
})

router.get('/getintroscreens', (req, res) => {
  helper.getintroscreens((response) => {
    res.send(response)
    log(`/getintroscreens,status:${response.status}`)
  })
})

router.get('/getshopbannersupdatenonce', (req, res) => {
  helper.getshopbannersupdatenonce((response) => {
    res.send(response)
  })
})

router.get('/getshopbanners', (req, res) => {
  helper.getshopbanners((response) => {
    res.send(response)
  })
})

router.get('/getshopbycategory', (req, res) => {
  helper.getshopbycategory((response) => {
    res.send(response)
  })
})

router.get('/getbrands', (req, res) => {
  helper.getbrands((response) => {
    res.send(response)
  })
})

router.get('/getformats', (req, res) => {
  helper.getformats((response) => {
    res.send(response)
  })
})

//handled missing :walletid
router.get('/getpartners', (req, res) => {
  res.send([])
})

router.get('/:walletid/getpartners', (req, res) => {
  let walletid = req.params.walletid
  helper.getwalletpartners(walletid, (response) => {
    res.send(response)
  })
})

router.get('/getwalletpartners', (req, res) => {
  helper.getpartners((response) => {
    res.send(response)
  })
})

router.get('/getwalletreport', (req, res) => {
  let fphash = req.query.fph
  helper.getwalletreport(fphash, (response) => {
    if (response.status === 'success') {
      res.setHeader('Content-disposition', `attachment; filename=${response.filename}`)
      res.set('Content-Type', 'text/csv')
      res.send(response.data)
      return
    }
    res.send(response)
  })
})

router.get('/getshopwidgetupdatenonce', (req, res) => {
  helper.widgetupdatenonce((response) => {
    res.send(response)
  })
})

router.get('/getshopwidgets', (req, res) => {
  helper.getshopwidgets(req.query, (response) => {
    res.send(response)
    log(`/getshopwidgets/?section=${req.query.section || 'shop'}, status:${response.status}`)
  })
})

router.get('/gethamburger', (req, res) => {
  helper.gethamburger((response) => {
    res.send(response)
    log(`/gethamburger, status:${response.status}`)
  })
})

router.get('/checkscratchcards', (req, res) => {
  res.send({
    status: 'success',
    isvisible: process.env.IS_REWARDS_VISIBLE === 'true' ? true : false,
    isonlineshopvisible: process.env.IS_ONLINE_SHOP_VISIBLE === 'true' ? true : false
  })
})

router.get('/getshoponline', (req, res) => {
  helper.getshoponline((response) => {
    res.send(response)
    log(`/getshoponline, status:${response.status}`)
  })
})

module.exports = router
