const express = require('express')
const examplerouter = express.Router()
let counters = require('../sys/counters')

// CHECK CACHE MIDDLEWARE

// REAL VERSION
examplerouter.get('/', (req, res, next) => {
  let realdata = 'This is mock data'
  res.send(realdata)
})

module.exports = examplerouter
