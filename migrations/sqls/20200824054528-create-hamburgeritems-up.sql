CREATE TABLE IF NOT EXISTS `hamburgeritems` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `position` tinyint(4) DEFAULT '0',
  `icon` varchar(127) NOT NULL,
  `name` varchar(127) NOT NULL,
  `subscript` varchar(127) DEFAULT NULL,
  `tag` varchar(255) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `channeltype` varchar(45) DEFAULT NULL,
  `redirectto` varchar(255) DEFAULT NULL,
  `moengageevent` varchar(64) DEFAULT NULL,
  `eventattributes`json DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;