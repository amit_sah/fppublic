const mysql = require('../connectors/mysql')
const log = require('../sys/log')
const sysredis = require('../connectors/sysredis')
const _ = require('lodash')

class StorePersistence {
  executegetstorequery(city, callback) {
    const query =
      'SELECT storecode, name, address, city, state, longitude, latitude from stores WHERE city = ? AND isactive = ? AND enablelocator = ?'
    mysql.query(query, [city, 'Y', 'Y'], (err, resp) => {
      callback(err, resp)
    })
  }

  executegetpmstorequery(city, callback) {
    const query = 'SELECT storecode, name from stores WHERE city = ? AND isactive = ? and pricematch = ? AND enablelocator = ?'
    mysql.query(query, [city, 'Y', 'Y', 'Y'], (err, resp) => {
      callback(err, resp)
    })
  }

  getcities(callback) {
    const query = 'SELECT distinct city from stores WHERE isactive = ? AND enablelocator = ?'
    mysql.query(query, ['Y', 'Y'], (err, resp) => {
      callback(err, resp)
    })
  }

  getstorestate(callback) {
    const query = 'SELECT distinct state FROM stores WHERE isactive = ? AND state != ? order by state;'
    mysql.query(query, ['Y', ''], (err, resp) => {
      callback(err, resp)
    })
  }

  getpmcities(callback) {
    const query = 'SELECT distinct city from stores WHERE isactive = ? and pricematch = ? AND enablelocator = ?'
    mysql.query(query, ['Y', 'Y', 'Y'], (err, resp) => {
      callback(err, resp)
    })
  }

  getstoresbylocation(req, callback) {
    const redis = sysredis.getReadClient()
    //optional sent by user
    let rangeInKm = process.env.REDIS_GEO_DEFAULT_RADIUS
    if (req.query.range) {
      rangeInKm = req.query.range
    }
    redis.georadius(
      process.env.REDIS_GEO_STORE,
      req.query.longitude,
      req.query.latitude,
      rangeInKm,
      'km',
      (err, data) => {
        if (data && data.length == 0) {
          log(
            `No store has found in radius of ${rangeInKm} km from longitude ${req.query.longitude}, latitude ${
            req.query.latitude
            }`,
            true
          )
        }
        callback(err, data)
      }
    )
  }

  getstoresbylocationv2(req, callback) {
    const redis = sysredis.getReadClient()
    //optional sent by user
    let rangeInKm = Number(process.env.REDIS_GEO_DEFAULT_RADIUS)
    if (req.query.range) {
      rangeInKm = Number(req.query.range)
    }
    redis.georadius(
      process.env.REDIS_GEO_STORE,
      req.query.longitude,
      req.query.latitude,
      rangeInKm,
      'km',
      (err, resp) => {
        const response = { status: "failed", format: req.query.format, locatorlink: null, stores: [] }
        response.locatorlink = process.env[`${req.query.format}_LOCATOR_URL`] || null
        if (resp && resp.length) {
          try {
            resp.forEach(store => {
              const parsestore = JSON.parse(store)
              if (parsestore && parsestore.name && parsestore.format && parsestore.format.split("-")[0] == req.query.format) {
                response.stores.push(_.omit(parsestore, ['format', 'storecode']))
              }
            })
            if (response.stores.length) {
              response.status = "success"
              response.locatorlink = null
            } else {
              log(`format:${req.query.format}, no stores in radius of ${rangeInKm} km from lat:${req.query.latitude}, long:${req.query.longitude}`, true)
            }
            callback(null, response)
          } catch (e) {
            log(`Get geo store V2, parse error: ${e}`, true)
            callback(e, response)
          }
        } else {
          callback(err, response)
        }
      })
  }
}

module.exports = new StorePersistence()
