const sysredis = require('../connectors/sysredis')

module.exports = {
  getotp(mobile, device, callback) {
    callback(null, { mobile, device, message: 'OTP sent' })
  },

  isRegistered(mobile, callback) {
    const isRegistered = false
    callback(null, mobile, isRegistered)
  },

  setbit(runid, batchcount) {
    return new Promise((resolve, reject) => {
      const redis = sysredis.getReadClient()
      redis.setbit(`${process.env.REDIS_TASKS}:${runid}`, batchcount, 1, (err) => {
        if (err) {
          log(`REDIS Error ${err}`, true)
          reject(err)
        } else resolve(null)
      })
    })
  },

  unsetbit(runid, batchcount) {
    return new Promise((resolve, reject) => {
      const redis = sysredis.getReadClient()
      redis.setbit(`${process.env.REDIS_TASKS}:${runid}`, batchcount, 0, (err) => {
        if (err) {
          log(`REDIS Error ${err}`, true)
          reject(err)
        } else resolve(null)
      })
    })
  },

  getbit(runid, batchcount) {
    return new Promise((resolve, reject) => {
      const redis = sysredis.getReadClient()
      redis.getbit(`${process.env.REDIS_TASKS}:${runid}`, batchcount, (err, res) => {
        if (err) {
          log(`REDIS Error ${err}`, true)
          reject(err)
        } else resolve(res)
      })
    })
  }
}
