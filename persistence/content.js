const mysql = require('../connectors/mysql')
const log = require('../sys/log')
const sysredis = require('../connectors/sysredis')

class Content {
  getbanners(callback) {
    let query =
      "SELECT * from banners WHERE deleted = 'N' AND isactive = 'Y' and (type = 'public' OR type = 'scratchcard' OR type IS NULL)"

    mysql.query(query, [], (err, res) => {
      callback(err, res)
    })
  }

  getkritterbanners(callback) {
    let query =
      "SELECT * from banners WHERE deleted = 'N' AND isactive = 'Y' and (type = 'personalized automated' AND type IS NOT NULL)"

    mysql.query(query, [], (err, res) => {
      callback(err, res)
    })
  }

  getintroscreens(callback) {
    let query = "SELECT * from introscreens WHERE deleted = 'N' AND isactive = 'Y'"
    mysql.query(query, [], (err, res) => {
      callback(err, res)
    })
  }

  getshopbanners(callback) {
    let query = "SELECT * from shop WHERE deleted = 'N' AND isactive = 'Y'"
    mysql.query(query, [], (err, res) => {
      callback(err, res)
    })
  }

  getbrands(callback) {
    let query = "SELECT * from brands WHERE deleted = 'N' AND isactive = 'Y'"
    mysql.query(query, [], (err, res) => {
      callback(err, res)
    })
  }

  getshopbycategory(callback) {
    let query = "SELECT id,name,seqnumber,type,imagelink,htmllink,pwalink,deeplink,isactive,deleted,channeltype,categorysubtype from shopcategories WHERE deleted = ? AND isactive = ? AND (version = 'v1' OR version IS NULL);"
    mysql.query(query, ["N", "Y"], (err, res) => {
      callback(err, res)
    })
  }

  getbannersequences(callback) {
    let redis = sysredis.getClient()
    redis.get('fp:banners:sequences', (err, res) => {
      callback(err, res)
    })
  }

  getbannersupdatenonce(callback) {
    let redis = sysredis.getClient()
    redis.get('fp:banners:updatenonce', (err, res) => {
      callback(err, res)
    })
  }

  getshopbannersupdatenonce(callback) {
    let redis = sysredis.getClient()
    redis.get('fp:banners:shop:updatenonce', (err, res) => {
      callback(err, res)
    })
  }

  setbannerdata(incoming, callback) {
    const creativeid = incoming.creativeid
    const swipe = incoming.swipe
    const tap = incoming.tap
    const query = `UPDATE banners set swipe = swipe + ?, tap = tap + ?, updated_at = CURDATE() WHERE id = ?`
    let params = [swipe, tap, creativeid]
    mysql.query(query, params, (err, res) => {
      callback(err, res)
    })
  }
  getwalletreport(fpdetails, callback) {
    const query = `Select l.created_at as time, l.subtype as activity, l.txnid, l.walletid, s.format, m.name as wallet_name, l.balance, concat( s.name, ', ', s.address) as store,
    CASE WHEN l.txntype = 'debit'
    THEN l.amount ELSE 0 END as "debit", CASE
    WHEN l.txntype = 'credit' THEN l.amount ELSE 0 END as "credit" FROM ledger l
    LEFT JOIN metawallets m on m.walletid = l.walletid
    LEFT JOIN stores s on l.storecode = s.storecode WHERE l.fpaccountid = ?
    and l.id BETWEEN (select min_ledger_id from minledgerid WHERE year=? and month=?) and
    (select min_ledger_id-1 from minledgerid WHERE year=? and month=?)
    and l.state in ( 'success','completed','pending','debited' )
    order by walletid, l.id ASC`
    let params = [fpdetails.fpid, fpdetails.oyr, fpdetails.omnt, fpdetails.cyr, fpdetails.cmnt]
    mysql.query(query, params, (err, res) => {
      log(`${res.length} entries found`, false)
      callback(err, res)
    })
  }

  widgetupdatenonce(callback) {
    let redis = sysredis.getClient()
    redis.get('fp:widget:updatenonce', (err, res) => {
      callback(err, res)
    })
  }

  getwidgetlist(section, callback) {
    const query = `SELECT id,name,seqnumber,type,headertext,buttontext,buttoncategorytype,buttoncategorylink,category,channeltype,subtype,section from shopwidgets WHERE isactive = ? AND deleted = ? AND subtype = ? AND section = ? ORDER BY seqnumber;`
    mysql.query(query, ["Y", "N", "public", section], (err, res) => {
      callback(err, res)
    })
  }
  getsubwidgets_by_widgetids(widgetids, section, callback) {
    const query = `SELECT id,name,seqnumber,categorysubtype,widgetid,imagelink,htmllink,pwalink,deeplink,channeltype,campaignname,type,tags,subtype,section, external from shopcategories WHERE  isactive = ? AND deleted = ? AND version = ? AND subtype = ? AND section = ? AND widgetid IN(${widgetids.join(',')}) ORDER BY widgetid, seqnumber;`
    mysql.query(query, ["Y", "N", "v2", "public", section], (err, resp) => {
      callback(err, resp)
    })
  }

  getshoponline(callback) {
    const query = `SELECT id,name,categorysubtype,htmllink,pwalink,deeplink,channeltype,campaignname,tags,toolbartext from shopcategories WHERE isactive = 'Y' AND deleted = 'N' AND type = 'button' AND section = 'shoponline';`
    mysql.query(query, [], (err, resp) => {
      callback(err, resp)
    })
  }

  gethamburger(callback) {
    let query = "SELECT id,name,position,icon,subscript,tag,type,channeltype,redirectto,moengageevent,eventattributes from hamburgeritems WHERE isactive = 'Y' AND deleted = 'N'"
    mysql.query(query, [], (err, res) => {
      callback(err, res)
    })
  }
}

module.exports = new Content()
