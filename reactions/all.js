const async = require('async')
const _ = require('lodash')
const protoroot = require('../protobuf/build')
const log = require('../sys/log')

module.exports = (event, _incoming, callback) => {
  let [ignore, incoming] = [null, null]

  // Add service specific checks for ignoring event
  function ignoreevent(_callback) {
    _callback(ignore)
  }

  function decodeproto(_callback) {
    const protopackage = _.get(protoroot, event)
    incoming = protopackage['event'].decode(_incoming)
    // log(incoming) //Remove this log from your service when you extend this file
    _callback(null)
  }

  async.waterfall([ignoreevent, decodeproto], err => {
    if (ignore || !err) callback(null)
    else callback(new Error(err))
  })
}
