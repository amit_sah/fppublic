//This can also be you require variable
const request = require('request')
const metaservice = require('../persistence/metaservice')
const log = require('../sys/log')
const getData = function () {
  request.get('www.get.service?exampleparam=2', (err, response, data) => {
    log({ err, response, data })
  })
}

module.exports = (input, _cb) => {
  request.get('www.get.service?exampleparam=1', (err, response, data) => {
    log({ err, response, data })
  })
  getData()
  request.post('www.ff.aa/getotp', { expected: 'request' }, (err, response, data) => {
    log({ err, response, data })
  })
  request.post('www.ff.aa/register', { expected: 'request' }, (err, response, data) => {
    log({ err, response, data })
  })
  metaservice.getotp(1787456545, 'android', (err, data) => {
    log({ err, data })
  })
  //exec callback
  _cb(null, false) //no error so exec callback
}
