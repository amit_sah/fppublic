const { Consumer } = require('sqs-consumer')
const AWS = require('aws-sdk')
const log = require('./log')
const WORKERS_SLEEP_TIME = process.env.WORKERS_SLEEP_TIME || 5000
const WORKERS_HANDLE_MESSAGE_TIMEOUT = process.env.WORKERS_HANDLE_MESSAGE_TIMEOUT || 600000
const amqp = require('amqp-connection-manager')
const Metaservice = require('../persistence/metaservice')

let workerlock = true

module.exports = {
  parseincoming(_incoming) {
    return new Promise((resolve, reject) => {
      try {
        let incoming = JSON.parse(_incoming.Body)
        resolve(incoming)
      } catch (err) {
        reject(err)
      }
    })
  },

  handleMessage(queuename, handler) {
    if (process.env.ENABLE_RABBITMQ_TASKS === 'true') {
      this.rabbitmqhandler(queuename, handler)
    } else {
      // Fallback to SQS
      this.sqshandler(queuename, handler)
    }
  },

  rabbitmqhandler(queuename, handler) {
    const heartbeat = Number(process.env.RABBITMQ_HEARTBEAT) || 60
    let rabbitmqworkerconn, rabbitmqworkerchannel
    const queueconfig = [
      `amqp://${process.env.RABBITMQ_USER}:${process.env.RABBITMQ_PASSWORD}@${
        process.env.RABBITMQ_HOST_TASKS
      }?heartbeat=${heartbeat}`
    ]

    rabbitmqworkerconn = amqp.connect(queueconfig)
    rabbitmqworkerconn.on('connect', () => log(`Worker subscriber has connected.`.cyan.bold))
    rabbitmqworkerconn.on('disconnect', params => {
      log(`Workersubscriber has disconnected.`.cyan, true)
      log(params.err.stack, true, false)
    })
    rabbitmqworkerconn.on('error', error => {
      log(`Worker subscriber connection catch unhadled error.`.red, true)
      log(error, true, false)
    })

    rabbitmqworkerchannel = rabbitmqworkerconn.createChannel({
      setup: chn => {
        chn.on('error', err => {
          log(`Worker subscriber channel has catch unhandled error.`.cyan, true)
          log(err, true, true)
        })

        const bindSubcriberEvent = [
          //Bind queue consumer
          chn.assertQueue(queuename, {
            exclusive: process.env.RABBITMQ_OPT_EXCLUSIVE == 'true' ? true : false,
            durable: process.env.RABBITMQ_OPT_DURABLE == 'true' ? true : false,
            autoDelete: process.env.RABBITMQ_OPT_QUEUE_DEL == 'true' ? true : false
          }),

          chn.prefetch(Number(process.env.RABBITMQ_WORKER_PREFETCH) || 1),

          //Bind listener
          chn.consume(queuename, async msg => {
            let incoming, runid, batchcount
            if (workerlock) {
              let sleep = ms => new Promise(resolve => setTimeout(resolve, ms))
              log(`Sleeping worker ${queuename} for ${WORKERS_SLEEP_TIME} ms`)
              await sleep(WORKERS_SLEEP_TIME)
              log(`Resuming Worker ..`)
              workerlock = false
            }

            try {
              incoming = await this.parseincoming({ Body: msg.content })
              runid = incoming.processid
              batchcount = incoming.batchcount
              let isprocessed = await Metaservice.getbit(runid, batchcount)

              if (Boolean(isprocessed)) {
                log(`Batch ${batchcount} already processed ..`)
                chn.ack(msg)
              } else {
                await Metaservice.setbit(runid, batchcount)
                await handler(incoming)
                chn.ack(msg)
              }
            } catch (error) {
              if (runid) await Metaservice.unsetbit(runid, batchcount)
              log(error, true)
              chn.nack(msg)
            }
          })
        ]
        return Promise.all(bindSubcriberEvent)
      }
    })

    rabbitmqworkerchannel
      .waitForConnect()
      .then(function() {
        log(`Worker${queuename} subscriber has listening.`.green.bold)
      })
      .catch(err => {
        log(`Worker${queuename} subscriber refused to consume message.`, true)
        log(err.stack, true, true)
      })
  },

  sqshandler(queuename, handler) {
    let batchcounter = 0
    let messagecounter = 0
    let workerlock = true
    let batchrecedcounter = 0
    let incoming, runid, batchcount

    const app = Consumer.create({
      queueUrl: `https://sqs.${process.env.AWS_REGION}.amazonaws.com/${
        process.env.AWS_ACCOUNT_NUMBER
      }/${queuename}.fifo`,
      handleMessage: async message => {
        log(`Pulled from SQS ${++batchrecedcounter}`)

        if (workerlock) {
          let sleep = ms => new Promise(resolve => setTimeout(resolve, ms))
          log(`Sleeping for ${WORKERS_SLEEP_TIME} ms`)
          await sleep(WORKERS_SLEEP_TIME)
          log(`Resuming Worker ..`)
          workerlock = false
        }

        incoming = await this.parseincoming(message)
        runid = incoming.processid
        batchcount = incoming.batchcount

        let isprocessed = await Metaservice.getbit(runid, batchcount)

        if (Boolean(isprocessed)) {
          log(`Batch ${batchcount} already processed ..`)
        } else {
          await Metaservice.setbit(runid, batchcount)
          await handler(incoming)
        }
      },
      batchSize: 10, // 1 - 10
      sqs: new AWS.SQS(),
      handleMessageTimeout: WORKERS_HANDLE_MESSAGE_TIMEOUT, // Time to wait for handleMessage to finish before throwing timeout_error
      waitTimeSeconds: 5
    })

    app.on('stopped', () => {
      log(`Stopped in consumer for SQS Queue ${queuename}`, true)
    })

    app.on('empty', () => {
      //console.log('Message counter: ', messagecounter)
      // console.log('Batch counter: ', batchcounter)
      // log(`Empty in consumer for SQS Queue ${queuename} `, true)
    })

    app.on('error', err => {
      log(`Error in consumer for SQS Queue ${queuename} : ${err.message}`, true)
    })

    app.on('processing_error', async err => {
      log(`Processing Error in consumer for SQS Queue ${queuename} : ${err.message}`, true)
      if (runid) await Metaservice.unsetbit(runid, batchcount)
    })

    app.on('timeout_error', err => {
      log(`Timeout Error in consumer for SQS Queue ${queuename} : ${err.message}`, true)
    })

    app.on('message_processed	', message => {
      ++messagecounter
      log(`Message processed for SQS Queue ${queuename}`)
    })

    app.on('response_processed', () => {
      ++batchcounter
      log(`Batch processed for SQS Queue ${queuename}`)
    })

    app.start()
  }
}
