const protoroot = require('../protobuf/build')
const log = require('./log')
const _ = require('lodash')
const pjson = require('../package.json')
const amqp = require('amqp-connection-manager')
const uuidv1 = require('uuid/v1')
const counters = require('./counters')
const workers = require('../tasks')

let rabbitmqpubconn, rabbitmqpubchannel, rabbitmqsenderchannel
let rabbitmqtasksenderconn, rabbitmqtasksenderchannel //Seprate task's connection & channel

log(`Is RabbitMQ flag enabled?: ${process.env.ENABLE_RABBITMQ}`)
if (process.env.ENABLE_RABBITMQ == `true`) {
  const heartbeat = process.env.RABBITMQ_HEARTBEAT ? Number(process.env.RABBITMQ_HEARTBEAT) : 60 // default 60 seconds

  let queueconfig = [
    `amqp://${process.env.RABBITMQ_USER}:${process.env.RABBITMQ_PASSWORD}@${
      process.env.RABBITMQ_HOST
    }?heartbeat=${heartbeat}`
  ]
  if (process.env.RABBITMQ_HOSTS) {
    let hosts = process.env.RABBITMQ_HOSTS.split(',')
    if (hosts && hosts.length) {
      hosts = _.shuffle(hosts) //shuffle IPs to help in load balance
      queueconfig = hosts.map(
        host => `amqp://${process.env.RABBITMQ_USER}:${process.env.RABBITMQ_PASSWORD}@${host}?heartbeat=${heartbeat}`
      )
    }
  }

  //futurepay exchange connection
  rabbitmqpubconn = amqp.connect(queueconfig)
  rabbitmqpubconn.on('connect', () => log(`Reaction publish's connection is created.`.yellow.italic))
  rabbitmqpubconn.on('disconnect', params => {
    log(`Reaction publisher connection has disconnected due to below errors.`, false, true)
    log(params.err.message, true, false)
  })
  rabbitmqpubconn.on('error', error => {
    log(`Reaction publisher connection catch error event.`.red, true)
    log(error, true, false)
  })
  rabbitmqpubconn.on('blocked', reason => {
    log(`Reaction pub connection stop due to low resources like RAM`.red, true)
    log(reason, true, false)
  })
  rabbitmqpubconn.on('unblocked', () => {
    log(`Reaction pub connection has resume the publishing`.green, null)
  })

  //futurepay exchange channel
  rabbitmqpubchannel = rabbitmqpubconn.createChannel({
    json: false,
    setup: ch => {
      ch.on(`error`, error => {
        log(`Reaction publisher channel catch unhandled error.`.red, true)
        log(error, true, false)
      })
      ch.assertExchange(process.env.RABBITMQ_EXCHANGE, 'topic', {
        durable: process.env.RABBITMQ_OPT_DURABLE == 'true' ? true : false
      })
      return ch
    }
  })

  //futurepay sendtoqueue channel
  rabbitmqsenderchannel = rabbitmqpubconn.createChannel({
    json: false,
    setup: chn => {
      chn.on(`error`, error => {
        log(`Reaction sender channel catch the unhandled.`.red, true)
        log(error, true, false)
      })
      return chn.assertQueue(pjson.name, {
        exclusive: process.env.RABBITMQ_OPT_EXCLUSIVE == 'true' ? true : false,
        durable: process.env.RABBITMQ_OPT_DURABLE == 'true' ? true : false,
        autoDelete: process.env.RABBITMQ_OPT_QUEUE_DEL == 'true' ? true : false
      })
    }
  })

  /**
   * @workers [] list of service's task
   * @task/job scheduled to process
   * @conditinal connection & channel created
   * Create connection
   */
  if (workers.length) {
    //task connection
    const queuetaskconfig = [
      `amqp://${process.env.RABBITMQ_USER}:${process.env.RABBITMQ_PASSWORD}@${
        process.env.RABBITMQ_HOST_TASKS
      }?heartbeat=${heartbeat}`
    ]
    rabbitmqtasksenderconn = amqp.connect(queuetaskconfig)
    rabbitmqtasksenderconn.on('connect', () => log(`Task sender's connection is created.`.cyan.bold))
    rabbitmqtasksenderconn.on('disconnect', params => {
      log(`Task publisher has disconnected.`.cyan, true)
      log(params.err.message, true, false)
    })
    rabbitmqtasksenderconn.on('error', error => {
      log(`Task sender connection catch error event.`.red, true)
      log(error, true, false)
    })
    rabbitmqtasksenderconn.on('blocked', reason => {
      log(`Task sender connection stop due to low resources like RAM`.red, true)
      log(reason, true, false)
    })
    rabbitmqtasksenderconn.on('unblocked', () => {
      log(`Task sender connection has resume the publishing`.green, null)
    })
    //tasks sendtoqueue channel
    rabbitmqtasksenderchannel = rabbitmqtasksenderconn.createChannel({
      json: true,
      setup: chn => {
        chn.on('error', err => {
          log(`Task sender channel catch unhandled error.`.red, true)
          log(err, true, true)
        })
        const bindSubcriberEvent = []
        _.each(workers, w => {
          bindSubcriberEvent.push(
            chn.assertQueue(`${pjson.name}_${w}${process.env.SQS_POSTFIX}`, {
              exclusive: process.env.RABBITMQ_OPT_EXCLUSIVE == 'true' ? true : false,
              durable: process.env.RABBITMQ_OPT_DURABLE == 'true' ? true : false,
              autoDelete: process.env.RABBITMQ_OPT_QUEUE_DEL == 'true' ? true : false
            })
          )
        })
        return Promise.all(bindSubcriberEvent)
      }
    })
  }
}

module.exports = {
  /**
   * @method Publish wire msg over the channels/routing key
   * @param {string} reactionkey Rabbitmq routing key name
   * @param {object} message object which maps into protobuf declaration. NO NEED TO encode into binary
   * @param {function} _onpublish optional callback
   */
  publish(reactionkey, message, _onpublish) {
    if (process.env.ENABLE_COUNTERS == 'true') counters.incrementpublishcount()
    const protopackage = _.get(protoroot, reactionkey)
    const protomsg = protopackage['event'].create(message)
    const binary = protopackage['event'].encode(protomsg).finish()
    //Addional msg tag info
    const option = {
      headers: { retry: 0, requeue: false }, //Retry can be used on requeue upto max attempt
      messageId: uuidv1(), //unique msg id
      appId: pjson.name, //Publisher name can be identified on consumer end
      timestamp: new Date().getTime() //publish time
    }
    rabbitmqpubchannel
      .publish(process.env.RABBITMQ_EXCHANGE, reactionkey, binary, option)
      .then(status => {
        if (_onpublish) _onpublish(status)
        if (process.env.ENABLE_COUNTERS == 'true') counters.decrementpublishcount()
      })
      .catch(err => {
        log(`message publish has failed.`, true)
        log(err.message, true, true)
        //execute cb
        if (_onpublish) _onpublish(err)
        if (process.env.ENABLE_COUNTERS == 'true') counters.decrementpublishcount()
      })
  },

  /**
   * Forward RabbitMQ Publisher to gracefulexit via comms, servicewrapper
   * Publisher's reactionkey will close once the `eastwest grpc` get shutdown.
   * GRPC services used RabbitMQ to publish message to the `exchange`
   */
  getrabbitmqpublisher() {
    return { rabbitmqpubchannel, rabbitmqpubconn }
  },

  /**
   * Republish on `requeue`== true from consumer/subscriber via comms
   * @param {*} reactionkey msg.fields.routingKey
   * @param {*} binary msg.content `Binary`
   * @param {*} option  {headers:{retry,requeue,routingKey,exchange}, messageId}
   * @param {*} _onpublish callback optional
   */
  republish(reactionkey, binary, option, _onpublish) {
    if (process.env.ENABLE_COUNTERS == 'true') counters.incrementpublishcount()
    //Additional info
    option.timestamp = new Date().getTime() //sending time
    option.appId = pjson.name //source publisher name

    rabbitmqsenderchannel
      .sendToQueue(pjson.name, binary, option)
      .then(status => {
        if (_onpublish) _onpublish(status)
        if (process.env.ENABLE_COUNTERS == 'true') counters.decrementpublishcount()
      })
      .catch(err => {
        log(`sendToQueue has failed.`, true)
        log(err.message, true, true)
        //execute cb
        if (_onpublish) _onpublish(err)
        if (process.env.ENABLE_COUNTERS == 'true') counters.decrementpublishcount()
      })
  },

  /**
   * Task's message sender
   * @param {*} reactionkey
   * @param {*} binary
   * @param {*} option
   * @param {*} _onpublish
   */
  sendtoqueue(queuename, msg, option = {}, _onpublish) {
    if (process.env.ENABLE_COUNTERS == 'true') counters.incrementpublishcount()
    //Additional info
    option.messageId = uuidv1() //unique msg id
    option.timestamp = new Date().getTime() //sending time
    option.appId = pjson.name //source publisher name

    rabbitmqtasksenderchannel
      .sendToQueue(queuename, msg, option)
      .then(status => {
        if (_onpublish) _onpublish(status)
        if (process.env.ENABLE_COUNTERS == 'true') counters.decrementpublishcount()
      })
      .catch(err => {
        log(`Task publish message has failed.`, true)
        log(err.message, true, true)
        //execute cb
        if (_onpublish) _onpublish(err)
        if (process.env.ENABLE_COUNTERS == 'true') counters.decrementpublishcount()
      })
  }
}
