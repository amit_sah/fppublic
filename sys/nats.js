const NATS = require('nats')
const protoroot = require('../protobuf/build')
const log = require('./log')
const _ = require('lodash')
const uuidv4 = require('uuid/v4')
const microtime = require('microtime')
let sysnats, rxnats

if (!process.env.ENABLE_RABBITMQ || process.env.ENABLE_RABBITMQ == `false`) {
  // creating array of sysnats servers.
  const sysnatsservers = {
    servers: process.env.SYSNATS_HOSTS
      ? process.env.SYSNATS_HOSTS.split(',').map(
          host =>
            `nats://${process.env.SYSNATS_USER}:${process.env.SYSNATS_PASSWORD}@${host}:${process.env.SYSNATS_PORT}`
        )
      : [
          `nats://${process.env.SYSNATS_USER}:${process.env.SYSNATS_PASSWORD}@${process.env.SYSNATS_HOST}:${
            process.env.SYSNATS_PORT
          }`
        ]
  }
  // creating array of rxnats servers.
  const rxnatsservers = {
    servers: process.env.RXNATS_HOSTS
      ? process.env.RXNATS_HOSTS.split(',').map(
          host => `nats://${process.env.RXNATS_USER}:${process.env.RXNATS_PASSWORD}@${host}:${process.env.RXNATS_PORT}`
        )
      : [
          `nats://${process.env.RXNATS_USER}:${process.env.RXNATS_PASSWORD}@${process.env.RXNATS_HOST}:${
            process.env.RXNATS_PORT
          }`
        ]
  }
  //connecting to sysnats if ENABLE_SYSNATS exists
  sysnats =
    process.env.ENABLE_SYSNATS == 'true'
      ? NATS.connect({ ...sysnatsservers, preserveBuffers: true, encoding: 'binary' })
      : null
  //connecting to rxnats
  rxnats = NATS.connect({
    ...rxnatsservers,
    preserveBuffers: true,
    encoding: 'binary'
  })
}
module.exports = {
  /**
   * @function request
   * @param {string} channel NATS channel name
   * @param {object} message object which maps into protobuf declaration. NO NEED TO encode into binary
   * @param {object} context object for NATS diagnostics
   * @param {function} callback
   */
  request(channel, message, context, callback) {
    const protopackage = channel.split('/')
    const protomsg = protoroot[protopackage[0]][protopackage[1]]['request'].create(message)
    if (context && context.trail) {
      context.trail.push[{ servicename: protopackage[0], microtime: microtime.now(), isreq: true }] //TODO
    } else {
      context = {
        uuid: uuidv4(),
        trail: [{ servicename: protopackage[0], microtime: microtime.now(), isreq: true }]
      }
    }
    protomsg.ctx = context
    const binary = protoroot[protopackage[0]][protopackage[1]]['request'].encode(protomsg).finish()
    sysnats.requestOne(
      channel,
      binary,
      _.has(process.env, 'SYSNATS_TIMEOUT') ? parseInt(process.env.SYSNATS_TIMEOUT) : 5000,
      function(response) {
        if (response instanceof NATS.NatsError && response.code === NATS.REQ_TIMEOUT) {
          log(`NATS request for timed out for ${channel}`)
          callback('timeout')
        } else {
          const _response = protoroot[protopackage[0]][protopackage[1]]['response'].decode(response)
          callback(null, _response)
        }
      }
    )
  },

  //Publish
  /**
   * @param {string} channel NATS channel name
   * @param {object} message object which maps into protobuf declaration. NO NEED TO encode into binary
   */
  publish(channel, message) {
    const protopackage = _.get(protoroot, channel)
    const protomsg = protopackage['event'].create(message)
    const binary = protopackage['event'].encode(protomsg).finish()
    return rxnats.publish(channel, binary)
  }
}
