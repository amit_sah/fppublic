const log = require('./log')
let counters = require('./counters')
const pjson = require('../package.json')
const _ = require('lodash')
let [sysnats, rxnats, server, grpcnorthsouthserver, grpceastwestserver, getrabbitmqconfig] = [
  null,
  null,
  null,
  null,
  null,
  null
]

/**
 * eastwest channel counter staus
 * check untill channel has no pending request.
 */
function geteastweststatus() {
  log(`EastWest Pending requests: ${counters.getall().alleastwestchannels}`)
  if (counters.getall().alleastwestchannels < 1) {
    return true
  }
  return false
}

/**
 * northsouth channel counter status
 * check untill channel has no pending request.
 */
function getnorthsouthstatus() {
  log(`NorthSouth Pending requests: ${counters.getall().allnorthsouthchannels}`)
  if (counters.getall().allnorthsouthchannels < 1) {
    return true
  }
  return false
}

/**
 * RabbitMQ publisher counter status
 * check untill message get published
 */
function getrabbitmqpublishstatus() {
  log(`Queue message publish pending: ${counters.getall().rabbitmqpublishercounter}`)
  if (counters.getall().rabbitmqpublishercounter < 1) {
    return true
  }
  return false
}

/**
 * RXNATS reactions status
 * check untill no reactions exist.
 */
function getrxnatsstatus() {
  log(`RXNATS Pending requests: ${counters.getall().allreactions}`)
  if (counters.getall().allreactions < 1) {
    return true
  }
  return false
}

/**
 * Is gRPC native shutdown method, has trigger at least 1.
 * The native method `tryShutdown` will execute untill all request has process.
 */
function checkgrpccounter(count) {
  return count > 1 ? true : false
}

/*
 * Stop cache & DB servers
 * Call after gRPC, NATS, Express.close execute
 */
let releaseResources = _cb => {
  _.each(require('../connectors/cacheredis').getAllClient(), redis => redis.quit())

  log(`Cache Redis has closed.`.yellow.bold)
  require('../connectors/sysredis')
    .getClient()
    .quit()
  log(`SYSRedis has closed.`.yellow.bold)
  if (process.env.NODE_ENV === 'staging') {
    require('../connectors/streamredis')
      .getClient()
      .quit()
    log(`Stream Redis has closed.`.yellow.bold)
  }
  const mysql = require('../connectors/mysql')
  try {
    mysql.end(err => {
      if (err) {
        log(err, true, true)
      }
      log(`MySQL connection has closed.`.yellow.bold)
      _cb()
    })
  } catch (e) {
    log(e, true, true)
    _cb()
  }
}

/**
 * RabbitMQ Subscriber
 * @param {*} rabbitmqsubchannel
 * @param {*} rabbitmqsubconn
 */
const closerabbitmqsubscriber = (
  rabbitmqsuballchannel,
  rabbitmqsubchannel,
  rabbitmqsubconn,
  rabbitmqcacheinvalidchannel
) => {
  /**
   * Reject the available msgs from current subrober channels
   * Rejected msgs will transfer to other replica subriber of same queue `servicename`
   * Remains in queue, If replica does exist, will expire after `TTL` duration
   */
  rabbitmqsubchannel.nackAll()

  //Close all wildcard channels
  if (rabbitmqsuballchannel) {
    rabbitmqsuballchannel.nackAll()
    rabbitmqsuballchannel.close().then(() => {
      log(`Reaction global subscriber channel has closed.`.blue.italic)
    })
  }

  //Close delete cache channel
  if (rabbitmqcacheinvalidchannel) {
    rabbitmqcacheinvalidchannel.nackAll()
    rabbitmqcacheinvalidchannel.close()
  }
  //Close the subriber channels
  rabbitmqsubchannel
    .close()
    .then(() => {
      log(`Reaction subscriber's channel has closed.`.yellow.italic)
      return rabbitmqsubconn.close()
    })
    .then(() => {
      log(`Reaction subscriber's connection has closed.`.yellow.italic)
    })
    .catch(e => log(e.message, true, true))
}

/**
 * RabbitMQ Publisher
 * @param {*} rabbitmqsubchannel
 * @param {*} rabbitmqsubconn
 */
const closerabbitmqpublisher = (rabbitmqpubchannel, rabbitmqpubconn) => {
  //Close the subscriber channel & connection
  rabbitmqpubchannel
    .close()
    .then(() => {
      log(`Reaction publisher's channel has closed.`.yellow.italic)
      return rabbitmqpubconn.close()
    })
    .then(() => {
      log(`Reaction publisher's connection has released.`.yellow.italic)
      setTimeout(() => process.exit(), 2000)
    })
    .catch(e => log(e.message, true, true))
}

const gracefulShutdown = function () {
  log(`Service ${pjson.name} termination request received. Waiting for pending requests to finish.`.red.bold)
  let queueconfig = null
  if (process.env.ENABLE_RABBITMQ == `true` && getrabbitmqconfig) {
    //Get subriber connections & channels
    queueconfig = getrabbitmqconfig()
  }
  //Stop RabbitMQ subscriber
  if (queueconfig)
    closerabbitmqsubscriber(
      queueconfig.rabbitmqsuballchannel,
      queueconfig.rabbitmqsubchannel,
      queueconfig.rabbitmqsubconn,
      queueconfig.rabbitmqcacheinvalidchannel
    )

  //Stop sys reaction
  if (sysnats) {
    sysnats.unsubscribe()
    log(`SYSNATS has stopped publish.`.yellow.bold)
  }
  //Stop rx reaction
  if (rxnats) {
    rxnats.unsubscribe()
    log(`RXNATS NATS has stopped publish.`.yellow.bold)
  }

  //Initialize tryShutdown counter
  let grpcshutdowncount = 0
  grpceastwestserver.tryShutdown(() => {
    log(`gRPC EAST WEST has closed.`.yellow.bold)
    grpcshutdowncount++
  })

  grpcnorthsouthserver.tryShutdown(() => {
    log(`gRPC NORTH SOUTH has closed.`.yellow.bold)
    grpcshutdowncount++
  })

  /**
   * Graceful exit on `ENABLE_COUNTERS` enable in .env else force exit
   * Wait `terminate` untill all request has finished
   */
  if (process.env.ENABLE_COUNTERS == 'true') {
    log(`Service pid ${process.pid} graceful shutting down.`.yellow.bold)
    //Try shutdown on 1sec interval
    const interval = setInterval(() => {
      const iseastwestfinished = geteastweststatus()
      const isnorthsouthfinished = getnorthsouthstatus()
      const isqueuepublishfinished = getrabbitmqpublishstatus()

      //Close RabbitMQ publisher once grpc get sgutdown
      if (isqueuepublishfinished && grpcshutdowncount >= 2 && queueconfig) {
        closerabbitmqpublisher(queueconfig.rabbitmqpubchannel, queueconfig.rabbitmqpubconn)
      }

      if (isqueuepublishfinished && iseastwestfinished && isnorthsouthfinished && checkgrpccounter(grpcshutdowncount)) {
        clearInterval(interval)
        //Close Express server
        server.close(() => {
          log(`REST(Express) Server has closed.`.yellow.bold)
          //Stop cache, db servers.
          releaseResources(() => {
            log(`Service ${pjson.name} has gracefully exit.`.green.bold)
            //Exit node service
            setTimeout(() => process.exit(), 3000)
          })
        })
      }
    }, 2000)
  } else {
    /**
     * Force exit service
     */
    log(`Service pid ${process.pid} force shuting down.`.red.bold)
    //Close RabbitMQ publisher forcely
    if (queueconfig) closerabbitmqpublisher(queueconfig.rabbitmqpubchannel, queueconfig.rabbitmqpubconn)
    //Close Express server
    server.close(function () {
      log(`Express(REST) Server has closed.`.yellow.bold)
      releaseResources(() => {
        log(`Service  ${pjson.name} has forced to exit.`.green.bold)
        //Exit node service
        setTimeout(() => process.exit(), 3000)
      })
    })
  }
}

module.exports = (_sysnats, _rxnats, _server, _grpcnorthsouthserver, _grpceastwestserver, _getrabbitmqconfig) => {
  server = _server
  sysnats = _sysnats
  rxnats = _rxnats
  grpcnorthsouthserver = _grpcnorthsouthserver
  grpceastwestserver = _grpceastwestserver
  getrabbitmqconfig = _getrabbitmqconfig
  // register event on TERM signal .e.g. kill pid
  process.on('SIGTERM', gracefulShutdown)
  // register event on INT signal e.g. Ctrl-C
  process.on('SIGINT', gracefulShutdown)
}
