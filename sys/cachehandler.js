const cacheredis = require('../connectors/cacheredis')
const log = require('../sys/log')
const mapname = require('../package.json').name

class RedisCache {
  getprefix(channelrequest) {
    let prefix = ''
    if (channelrequest && channelrequest.customerid && channelrequest.token) {
      prefix = `${Number(channelrequest.customerid).toString(36)}:${channelrequest.token.substr(
        0,
        process.env.CACHEREDIS_TOKEN_MIN_LENGTH
      )}`
    }
    return prefix
  }

  getcache(prefix, fakekey, _callback) {
    if (prefix) {
      const redis = cacheredis.getClient(fakekey)
      if (redis.status !== 'ready') return _callback(`Wating for connection to happen for fakekey ${fakekey}`)
      let response = null
      log(`Get cached data for key : "${prefix}@${fakekey}"`)
      redis.hgetBuffer(mapname, `${prefix}@${fakekey}`, (err, res) => {
        if (err) log(`Cache get failed for fakekey '${prefix}@${fakekey}' in ${mapname}`, true)
        else response = res
        _callback(err, response)
        return err !== null
      })
    } else _callback(null, null)
  }

  setcache(prefix, fakekey, tocache, _callback) {
    if (prefix) {
      log(`Setting cache for key: ${mapname} : ${prefix}@${fakekey}`)
      const redis = cacheredis.getClient(fakekey)
      redis.hset(mapname, `${prefix}@${fakekey}`, tocache, err => {
        if (err) log(`Cache save failed for '${prefix}@${fakekey}' in '${mapname}'`, true)
        _callback(err)
        return err !== null
      })
    } else _callback(null)
  }

  deletecache(prefix, fakekey) {
    const redis = cacheredis.getClient(fakekey)
    // log(`hdel prefix@fakekey: "${prefix}@${fakekey}"`)
    redis.hdel(mapname, `${prefix}@${fakekey}`, err => {
      if (err) log(`Cache deletion failed for ${prefix}@${fakekey} in ${mapname}`, true)
      // else log(`Cache deleted if exists for ${prefix}@${fakekey} in ${mapname}`)
      return err !== null
    })
  }
}

module.exports = new RedisCache()
