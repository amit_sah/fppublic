const Redis = require('ioredis')
const log = require('../sys/log')

let cacheredisconnections = {}

const getClient = fakekey => {
  if (
    !cacheredisconnections[fakekey] ||
    !(
      cacheredisconnections[fakekey].status === 'connecting' ||
      cacheredisconnections[fakekey].status === 'connect' ||
      cacheredisconnections[fakekey].status === 'ready'
    )
  ) {
    let opts = {
      host: process.env[`CACHEREDIS_HOST_${fakekey}`],
      port: process.env[`CACHEREDIS_PORT_${fakekey}`] || 6379,
      password: process.env[`CACHEREDIS_PASSWORD_${fakekey}`] || process.env.CACHEREDIS_PASSWORD
    }
    cacheredisconnections[fakekey] = new Redis(opts)
    cacheredisconnections[fakekey].on(`error`, error => log(`Cache redis ${error}`, true))
  }
  return cacheredisconnections[fakekey]
}

let getAllClient = () => {
  return cacheredisconnections
}

module.exports = { getClient, getAllClient }
