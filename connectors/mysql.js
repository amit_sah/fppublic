const log = require('../sys/log')
const mysqlconfig = {
  host: process.env.MYSQL_HOST,
  port: process.env.MYSQL_PORT,
  user: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  database: process.env.MYSQL_DATABASE,
  connectionLimit: process.env.MYSQL_CONNECTION_LIMIT
}
const pool = require('mysql').createPool(mysqlconfig)
const mysqlexport = {
  query(query, params, callback) {
    if (process.env.LOG_MYSQL_LATENCY) console.time('mysql_latency')
    pool.getConnection((err, conn) => {
      if (err != null) {
        log(`[ERROR] Failed to getConnection from mySQL - ${err}`, true, true)
        if (conn != null) {
          conn.release()
        }
        if (callback != null) {
          if (process.env.LOG_MYSQL_LATENCY) console.timeEnd('mysql_latency')
          return callback('DB error')
        }
      }
      conn.query(query, params, (_err, rows) => {
        if (process.env.LOG_MYSQL_LATENCY) console.timeEnd('mysql_latency')
        if (_err != null) {
          log(_err, true)
        }
        conn.release()
        if (callback != null) {
          return callback(_err, rows)
        }
      })
    })
  },
  end(callback) {
    pool.end(callback)
  }
}
module.exports = mysqlexport
