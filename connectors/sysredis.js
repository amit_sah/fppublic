const Redis = require('ioredis')
const log = require('../sys/log')
const {
  SYSREDIS_HOST,
  SYSREDIS_HOSTS,
  SYSREDIS_PORT,
  SYSREDIS_OLD_PORT,
  SYSREDIS_SENTINEL_PORT,
  SYSREDIS_PASSWORD,
  SYSREDIS_READER_HOST,
  SYSREDIS_READER_HOSTS,
  SYSREDIS_READER_PORT,
  SYSREDIS_OLD_READER_PORT,
  SYSREDIS_SENTINEL_READER_PORT,
  SYSREDIS_READER_PASSWORD,
  SYSREDIS_CLUSTER_NAME,
  SYSREDIS_READER_CLUSTER_NAME
} = process.env
const opts = process.env.ENABLE_SENTINELS
  ? {
    sentinels: (SYSREDIS_HOSTS || SYSREDIS_HOST).split(',').map(host => {
      return {
        host,
        port: parseInt(SYSREDIS_SENTINEL_PORT || SYSREDIS_PORT)
      }
    }),
    name: SYSREDIS_CLUSTER_NAME || 'mymaster'
  }
  : {
    host: SYSREDIS_HOST,
    port: parseInt(SYSREDIS_OLD_PORT)
  }
opts.password = SYSREDIS_PASSWORD

const readopts = process.env.ENABLE_SENTINELS
  ? {
    sentinels: (SYSREDIS_READER_HOSTS || SYSREDIS_READER_HOST).split(',').map(host => {
      return {
        host,
        role: 'slave',
        port: parseInt(SYSREDIS_SENTINEL_READER_PORT || SYSREDIS_READER_PORT)
      }
    }),
    name: SYSREDIS_READER_CLUSTER_NAME || 'mymaster'
  }
  : {
    host: SYSREDIS_READER_HOST,
    port: parseInt(SYSREDIS_OLD_READER_PORT)
  }
readopts.password = SYSREDIS_READER_PASSWORD

let redis = null,
  readredis = null
const getClient = function () {
  if (!redis || !(redis.status === 'connecting' || redis.status === 'connect' || redis.status === 'ready')) {
    // log(`Renewing sysredis connection`)
    redis = new Redis(opts)
    redis.on(`error`, error => log(`Sys redis ${error}`, true))
  }
  return redis
}

const getReadClient = function () {
  if (
    !readredis ||
    !(readredis.status === 'connecting' || readredis.status === 'connect' || readredis.status === 'ready')
  ) {
    // log(`Renewing sysredis reader connection`)
    readredis = new Redis(readopts)
    readredis.on(`error`, error => log(`Sys redisr ${error}`, true))
  }
  return readredis
}

module.exports = { getClient, getReadClient }
