const Content = require('../persistence/content')
const async = require('async')
const _ = require('lodash')
const log = require('../sys/log')
module.exports = (incoming, context, callback) => {
    let clienterr, syserr

    const validateincoming = _callback => {
        if (incoming.creativeid === 0) {
            clienterr = syserr = 'Missing important data'
            log(syserr)
        }
        _callback(clienterr)
    }

    const setbannerdata = _callback => {
        Content.setbannerdata(incoming, (err, res) => {
            if (err) {
                syserr = clienterr = err
            }
            _callback(clienterr)
        })
    }

    async.waterfall([validateincoming, setbannerdata], err => {
        if (err || clienterr) {
            log(syserr || err, true)
            const message = new Error(clienterr || err)
            callback(message)
        } else {
            callback(null, { status: 'success' })
        }
    })
}